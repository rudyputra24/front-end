package com.bca.microservices1.frontendBLI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FrontendBliApplication {

	public static void main(String[] args) {
		SpringApplication.run(FrontendBliApplication.class, args);
	}

}
