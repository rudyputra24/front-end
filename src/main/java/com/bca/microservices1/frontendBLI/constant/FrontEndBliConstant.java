package com.bca.microservices1.frontendBLI.constant;

public class FrontEndBliConstant {
	private static final int PORT = 8080;

	public static final String HOST_BUS_FE = "http://localhost:"+ PORT +"/bus/list";
	public static final String HOST_COURSE_FE = "http://localhost:"+ PORT +"/course/list";
	public static final String HOST_LUNCH_FE = "http://localhost:"+ PORT +"/lunch/list?day=";

	private static final String BUS_SERVER = "http://10.1.129.154:8090/";
	private static final String COURSE_SERVER = "http://10.1.129.205:8080/";
	private static final String LUNCH_SERVER = "http://10.1.129.205:8080/";

	public static final String HOST_BUS_LIST = BUS_SERVER+"bus/";

	public static final String HOST_COURSE_DT_FE = "http://localhost:8080/course/detail";
	public static final String HOST_COURSE_GET_ALL = COURSE_SERVER+"/get-course";
	public static final String HOST_COURSE_GET_CRS = COURSE_SERVER+"/get-course-by-id";
	public static final String HOST_COURSE_GET_TRN = COURSE_SERVER+"/get-trainee-by-course";

	public static final String HOST_LUNCH_GET_MENU = LUNCH_SERVER+"/get-makanan-hari?hari=";
	public static final String HOST_LUNCH_BOOK = LUNCH_SERVER+"/book";
	public static final String HOST_LUNCH_FIND_BOOK = LUNCH_SERVER+"/find-book";
	public static final String HOST_LUNCH_CANCEL_BOOK = LUNCH_SERVER+"/cancel";

	public static final String FORMAT_D2M3Y4 = "dd MMM yyyy"; 
	public static final String FORMAT_Y4M2D2 = "yyyy-MM-dd";
}