package com.bca.microservices1.frontendBLI.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import com.bca.microservices1.frontendBLI.service.BusService;

@Controller
public class BusController {
	
	@Autowired private BusService busService;
	
	@GetMapping("/bus/list")
	public ModelMap dataCustomer() {
        return new ModelMap().addAttribute("dataBus", busService.listBus());
    }

}
