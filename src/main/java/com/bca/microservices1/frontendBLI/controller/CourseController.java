package com.bca.microservices1.frontendBLI.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.bca.microservices1.frontendBLI.constant.FrontEndBliConstant;
import com.bca.microservices1.frontendBLI.service.CourseService;

@Controller
public class CourseController {
	
	@Autowired private CourseService courseService;
	
	@GetMapping("/course/list")
	public ModelMap dataCourseList() {
		ModelMap model = new ModelMap();
		model.addAttribute("dataCourse", courseService.listCourse());
		model.addAttribute("detailCrsHost", FrontEndBliConstant.HOST_COURSE_DT_FE);
        return model;
    }
	
	@PostMapping("/course/detail")
	public ModelMap dataCourse(@RequestParam(name="id",required = false) Integer id) {
		ModelMap model = new ModelMap();
		model.addAttribute("dataCourseDetail", courseService.getCourse(id));
		model.addAttribute("traineeList", courseService.getListTrainee(id));
        
		return model;
		
	}
}
