package com.bca.microservices1.frontendBLI.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import com.bca.microservices1.frontendBLI.constant.FrontEndBliConstant;

@Controller
public class HomeController {
	
	@GetMapping("/main/menu")
	public ModelMap initPage() {
		ModelMap model = new ModelMap();
		
		model.addAttribute("host_bus", FrontEndBliConstant.HOST_BUS_FE);
		model.addAttribute("host_course", FrontEndBliConstant.HOST_COURSE_FE);
		model.addAttribute("host_lunch", FrontEndBliConstant.HOST_LUNCH_FE);
		
		return model;
	}
}
