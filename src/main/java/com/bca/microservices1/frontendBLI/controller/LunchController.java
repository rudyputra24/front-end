package com.bca.microservices1.frontendBLI.controller;

import com.bca.microservices1.frontendBLI.io.LunchBookRequest;
import com.bca.microservices1.frontendBLI.service.LunchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LunchController {
    @Autowired
    private LunchService lunchService;

    @GetMapping("/lunch/list")
    public ModelMap dataCustomer(@RequestParam String day) {
        return new ModelMap().addAttribute("dataMakanan", lunchService.listLunch(day));
    }
    
    @PostMapping("/lunch/book")
	public ModelMap viewBookForm(@RequestParam(name="id",required = false) Integer id) {
		ModelMap model = new ModelMap();
		LunchBookRequest lbReq = new LunchBookRequest();
		
		lbReq.setIdMenu(id);
        model.addAttribute("lbReq", lbReq);
		return model;
	}
    
    @PostMapping("/lunch/list")
    public ModelMap submitBookForm(@ModelAttribute("lbReq") LunchBookRequest lbReq) {
    	System.out.println("Id MENU : "+lbReq.getIdMenu());
    	System.out.println("Id User : "+lbReq.getIdUser());
    	
    	String output = lunchService.book(lbReq.getIdMenu(), lbReq.getIdUser());
    	ModelMap model = new ModelMap();
    	model.addAttribute("dataMakanan", lunchService.listLunch(""));
    	model.addAttribute("output", output);
    	
    	return model;
    }
}
