package com.bca.microservices1.frontendBLI.io;

import java.util.List;

import com.bca.microservices1.frontendBLI.io.entity.Bus;

import lombok.Data;

@Data
public class BusResponse {
	private String message;
	private List<Bus> busList;
}
