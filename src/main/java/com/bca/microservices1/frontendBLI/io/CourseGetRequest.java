package com.bca.microservices1.frontendBLI.io;

import lombok.Data;

@Data
public class CourseGetRequest {

	private int id;
}
