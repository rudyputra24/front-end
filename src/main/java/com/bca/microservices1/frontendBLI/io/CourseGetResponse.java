package com.bca.microservices1.frontendBLI.io;

import com.bca.microservices1.frontendBLI.io.entity.Course;

import lombok.Data;

@Data
public class CourseGetResponse {

	private Course data;
}
