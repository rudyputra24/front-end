package com.bca.microservices1.frontendBLI.io;

import java.util.List;

import com.bca.microservices1.frontendBLI.io.entity.Course;

import lombok.Data;

@Data
public class CourseListResponse {
	private List<Course> data; 
}
