package com.bca.microservices1.frontendBLI.io;

import com.bca.microservices1.frontendBLI.io.entity.Trainee;

import lombok.Data;

@Data
public class CourseTraineeResponse {

	private Trainee[] data;
}
