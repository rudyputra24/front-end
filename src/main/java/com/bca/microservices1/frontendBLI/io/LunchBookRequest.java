package com.bca.microservices1.frontendBLI.io;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LunchBookRequest {
    private int idUser;
    private int idMenu;
}
