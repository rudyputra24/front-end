package com.bca.microservices1.frontendBLI.io;

import lombok.Data;

@Data
public class LunchDefaultResponse {
    private String data;
    private int response_code;
    private String keterangan;
}
