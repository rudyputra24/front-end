package com.bca.microservices1.frontendBLI.io;

import com.bca.microservices1.frontendBLI.io.entity.Lunch;
import lombok.Data;

@Data
public class LunchListResponse {
    private Lunch[] data;
    private int response_code;
    private String keterangan;
}
