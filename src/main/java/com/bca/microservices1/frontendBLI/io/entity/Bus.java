package com.bca.microservices1.frontendBLI.io.entity;

import java.sql.Time;
import lombok.Data;

@Data
public class Bus {

	private int id_bus;
	private String name;
	private int seat_total;
	private String location;
	private Time departure_time;

}
