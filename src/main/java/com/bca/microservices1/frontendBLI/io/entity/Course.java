package com.bca.microservices1.frontendBLI.io.entity;

import lombok.Data;

@Data
public class Course {
	private int idCourse;
	private String subject;
	private String location;
	private int idTrainer;
	private String trainerName;
	private String courseDate;
}
