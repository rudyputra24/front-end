package com.bca.microservices1.frontendBLI.io.entity;

import lombok.Data;

@Data
public class Lunch {
    private int id;
    private String makanan;
    private int jumlah;
    private String hari;
}
