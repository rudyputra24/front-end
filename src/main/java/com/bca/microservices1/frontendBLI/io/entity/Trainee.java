package com.bca.microservices1.frontendBLI.io.entity;

import lombok.Data;

@Data
public class Trainee {
	
	private int id;
	private String nama;
	private String ttl;
	private String divisi;
	
}
