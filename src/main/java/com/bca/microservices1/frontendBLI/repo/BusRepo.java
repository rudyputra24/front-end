package com.bca.microservices1.frontendBLI.repo;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.bca.microservices1.frontendBLI.constant.FrontEndBliConstant;
import com.bca.microservices1.frontendBLI.io.BusResponse;
import com.bca.microservices1.frontendBLI.io.entity.Bus;

@Repository
public class BusRepo {
	
	public List<Bus> getBusList(){
		String url = FrontEndBliConstant.HOST_BUS_LIST;
		
		RestTemplate restTemp = new RestTemplate();
		ResponseEntity<BusResponse> busResponse = null;
		
		try {
			busResponse = restTemp.getForEntity(url, BusResponse.class);;
			List<Bus> listBus = busResponse.getBody().getBusList();
			
			for(Bus b:listBus) {
				System.out.println(b);
			}
			return listBus;
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("FAILED");
			return null;
		} finally {
			
		}
	}

}
