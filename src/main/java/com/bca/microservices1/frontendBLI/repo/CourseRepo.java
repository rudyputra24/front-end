package com.bca.microservices1.frontendBLI.repo;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.bca.microservices1.frontendBLI.constant.FrontEndBliConstant;
import com.bca.microservices1.frontendBLI.io.CourseGetRequest;
import com.bca.microservices1.frontendBLI.io.CourseGetResponse;
import com.bca.microservices1.frontendBLI.io.CourseListResponse;
import com.bca.microservices1.frontendBLI.io.CourseTraineeResponse;
import com.bca.microservices1.frontendBLI.io.entity.Course;
import com.bca.microservices1.frontendBLI.io.entity.Trainee;
import com.google.gson.Gson;

@Repository
public class CourseRepo {

	public List<Course> getCourseList() {
		// String url = FrontEndBliConstant.HOST_COURSE;
		String url = FrontEndBliConstant.HOST_COURSE_GET_ALL;

		RestTemplate restTemp = new RestTemplate();
		ResponseEntity<CourseListResponse> courseResponse = null;

		try {
			courseResponse = restTemp.getForEntity(url, CourseListResponse.class);
			;
			List<Course> listCourse = (List<Course>) courseResponse.getBody().getData();

			for (Course c : listCourse) {
				System.out.println(c);
			}
			return listCourse;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {

		}
	}

	public Course getCourse(CourseGetRequest crsReq) {

		String url = FrontEndBliConstant.HOST_COURSE_GET_CRS;

		RestTemplate restTemp = new RestTemplate();
		ResponseEntity<CourseGetResponse> courseResponse = null;

		System.out.println("Isi crsReq " + crsReq.getId());

		try {
			courseResponse = restTemp.postForEntity(url, crsReq, CourseGetResponse.class);
			Course crs = courseResponse.getBody().getData();

			System.out.println("Get Course " + crs.toString());

			return crs;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {

		}
	}

	public List<Trainee> getTraineeList(CourseGetRequest crsReq) {

		String url = FrontEndBliConstant.HOST_COURSE_GET_TRN;

		RestTemplate restTemp = new RestTemplate();
		ResponseEntity<CourseTraineeResponse> traineeResponse = null;

		try {
			
			System.out.println(crsReq.toString());
			
			traineeResponse = restTemp.postForEntity(url, crsReq, CourseTraineeResponse.class);
			List<Trainee> traineeList = Arrays.asList(traineeResponse.getBody().getData());
			
			Gson gson = new Gson();
			System.out.println(gson.toJson(traineeList));

			return traineeList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {

		}
	}

}
