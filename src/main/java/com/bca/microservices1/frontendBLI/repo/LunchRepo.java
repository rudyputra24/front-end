package com.bca.microservices1.frontendBLI.repo;

import com.bca.microservices1.frontendBLI.constant.FrontEndBliConstant;
import com.bca.microservices1.frontendBLI.io.LunchBookRequest;
import com.bca.microservices1.frontendBLI.io.LunchDefaultRequest;
import com.bca.microservices1.frontendBLI.io.LunchDefaultResponse;
import com.bca.microservices1.frontendBLI.io.LunchListResponse;
import com.bca.microservices1.frontendBLI.io.entity.Lunch;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class LunchRepo {
    public List<Lunch> getMakananList(String day){
        String url = FrontEndBliConstant.HOST_LUNCH_GET_MENU + day;

        RestTemplate restTemp = new RestTemplate();
        ResponseEntity<LunchListResponse> lunchResponseResponseEntity;

        try {
            lunchResponseResponseEntity = restTemp.getForEntity(url, LunchListResponse.class);;

            if (lunchResponseResponseEntity.getStatusCode() != HttpStatus.OK) {
                return null;
            }
            else {
                if (lunchResponseResponseEntity.getBody() != null) {
                    Lunch[] makanans = lunchResponseResponseEntity.getBody().getData();
                    return new ArrayList<>(Arrays.asList(makanans));
                }
                else {
                    return null;
                }
            }
        }catch(Exception e) {
            e.printStackTrace();
            System.out.println("FAILED");
            return null;
        }
    }

    public String book(int menu,int user){
        String url = FrontEndBliConstant.HOST_LUNCH_BOOK;

        RestTemplate restTemp = new RestTemplate();
        ResponseEntity<LunchDefaultResponse> lunchResponseResponseEntity;

        LunchBookRequest lunchBookRequest = new LunchBookRequest();
        lunchBookRequest.setIdMenu(menu);
        lunchBookRequest.setIdUser(user);

        try {
            lunchResponseResponseEntity = restTemp.postForEntity(url, lunchBookRequest, LunchDefaultResponse.class);;
            if (lunchResponseResponseEntity.getStatusCode() != HttpStatus.OK) {
                return null;
            }
            else {
                if (lunchResponseResponseEntity.getBody() != null) {
                    return lunchResponseResponseEntity.getBody().getData();
                }
                else {
                    return null;
                }
            }
        }catch(Exception e) {
            e.printStackTrace();
            System.out.println("FAILED");
            return null;
        }
    }

    private String _do(int user,String url) {
        RestTemplate restTemp = new RestTemplate();
        ResponseEntity<LunchDefaultResponse> lunchResponseResponseEntity;

        LunchDefaultRequest lunchDefaultRequest = new LunchDefaultRequest();
        lunchDefaultRequest.setIdUser(user);

        try {
            lunchResponseResponseEntity = restTemp.getForEntity(url, LunchDefaultResponse.class);;
            if (lunchResponseResponseEntity.getStatusCode() != HttpStatus.OK) {
                return null;
            }
            else {
                if (lunchResponseResponseEntity.getBody() != null) {
                    return lunchResponseResponseEntity.getBody().getData();
                }
                else {
                    return null;
                }
            }
        }catch(Exception e) {
            e.printStackTrace();
            System.out.println("FAILED");
            return null;
        }
    }

    public String cancel(int user){
        String url = FrontEndBliConstant.HOST_LUNCH_CANCEL_BOOK;
        return _do(user,url);
    }

    public String findBook(int user){
        String url = FrontEndBliConstant.HOST_LUNCH_FIND_BOOK;
        return _do(user,url);
    }
}
