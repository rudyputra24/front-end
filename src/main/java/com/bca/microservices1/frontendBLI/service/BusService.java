package com.bca.microservices1.frontendBLI.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bca.microservices1.frontendBLI.io.entity.Bus;
import com.bca.microservices1.frontendBLI.repo.BusRepo;

@Service
public class BusService {
	
	@Autowired private BusRepo busRepo;
	
	public List<Bus> listBus(){
		List<Bus> listBus = null;
		listBus = busRepo.getBusList(); 
		
		return listBus;
	}
	

}
