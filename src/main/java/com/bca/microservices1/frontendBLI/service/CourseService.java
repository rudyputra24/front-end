package com.bca.microservices1.frontendBLI.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bca.microservices1.frontendBLI.constant.FrontEndBliConstant;
import com.bca.microservices1.frontendBLI.io.CourseGetRequest;
import com.bca.microservices1.frontendBLI.io.entity.Course;
import com.bca.microservices1.frontendBLI.io.entity.Trainee;
import com.bca.microservices1.frontendBLI.repo.CourseRepo;

@Service
public class CourseService {

	@Autowired
	private CourseRepo courseRepo;

	public String convertPatternDateTime(String input, String oldPattern, String newPattern) {
		DateTimeFormatter oldPatternFormat = DateTimeFormatter.ofPattern(oldPattern);
		DateTimeFormatter newPatternFormat = DateTimeFormatter.ofPattern(newPattern);

		LocalDate datetime = LocalDate.parse(input, oldPatternFormat);

		return datetime.format(newPatternFormat);
	}

	public List<Course> listCourse() {
		List<Course> listCourse = null;
		listCourse = courseRepo.getCourseList();
		String output = "";

		for (Course c : listCourse) {
			output = convertPatternDateTime(c.getCourseDate(), FrontEndBliConstant.FORMAT_Y4M2D2,
					FrontEndBliConstant.FORMAT_D2M3Y4);
			c.setCourseDate(output);
		}

		return listCourse;
	}

	public Course getCourse(int courseId) {
		Course crs = null;
		CourseGetRequest crsReq = new CourseGetRequest();

		crsReq.setId(courseId);

		crs = courseRepo.getCourse(crsReq);
		String output = "";

		output = convertPatternDateTime(crs.getCourseDate(), FrontEndBliConstant.FORMAT_Y4M2D2,
				FrontEndBliConstant.FORMAT_D2M3Y4);
		crs.setCourseDate(output);

		return crs;
	}

	public List<Trainee> getListTrainee(int courseId) {
		List<Trainee> listTrainee = new ArrayList<Trainee>();
		CourseGetRequest crsReq = new CourseGetRequest();

		crsReq.setId(courseId);
		listTrainee = courseRepo.getTraineeList(crsReq);

		return listTrainee;
	}

}
