package com.bca.microservices1.frontendBLI.service;

import com.bca.microservices1.frontendBLI.io.entity.Lunch;
import com.bca.microservices1.frontendBLI.repo.LunchRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LunchService {
    @Autowired
    private LunchRepo lunchRepo;

    public List<Lunch> listLunch(String day){
        return lunchRepo.getMakananList(day);
    }

    public String book(int menu,int user) {
        return lunchRepo.book(menu,user);
    }

    public String findBook(int user) {
        return lunchRepo.findBook(user);
    }

    public String cancel(int user) {
        return lunchRepo.cancel(user);
    }
}
